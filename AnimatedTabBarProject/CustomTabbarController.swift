//
//  CustomTabbarController.swift
//  AnimatedTabBarProject
//
//  Created by Thomas Visser on 09/06/2020.
//  Copyright © 2020 Thomas Visser. All rights reserved.
//

import Foundation
import UIKit

class CustomTabbarController: UITabBarController, UITabBarControllerDelegate {
    
    private func animate(_ imageView: UIImageView) {
        UIView.animate(withDuration: 0.1, animations: {
            imageView.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }) { _ in
            UIView.animate(withDuration: 0.1, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 2.0, options: .curveEaseOut, animations: {
                imageView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.delegate = self
    }
    
    private var bounceAnimation: CAKeyframeAnimation = {
        let bounceAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        bounceAnimation.values = [1.0, 1.4, 0.8, 1.1, 1.0]
        bounceAnimation.duration = TimeInterval(0.2)
        bounceAnimation.calculationMode = CAAnimationCalculationMode.cubic
        return bounceAnimation
    }()
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        var imageView: UIImageView?
        
        if #available(iOS 13.0, *) {
            guard let index = tabBar.items?.index(of: item), tabBar.subviews.count > index+1 else {
                return
            }
                
            let visualEffectView = tabBar.subviews[index+1].subviews.first as? UIVisualEffectView
            imageView = visualEffectView?.contentView.subviews.compactMap({ $0 as? UIImageView }).first
        } else {
            guard let index = tabBar.items?.index(of: item), tabBar.subviews.count > index+1, let subView = tabBar.subviews[index+1].subviews.compactMap ({ $0 as? UIImageView }).first else {
                return
            }
            imageView = subView
        }
        
        guard let imageViewNotNil = imageView else {
            return
        }
        
        imageViewNotNil.layer.add(bounceAnimation, forKey: nil)
    }
}
