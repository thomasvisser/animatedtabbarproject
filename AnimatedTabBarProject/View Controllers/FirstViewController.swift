//
//  FirstViewController.swift
//  AnimatedTabBarProject
//
//  Created by Thomas Visser on 09/06/2020.
//  Copyright © 2020 Thomas Visser. All rights reserved.
//

import Foundation
import UIKit

class FirstViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .red
    }
}
